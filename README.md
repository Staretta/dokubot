Note: I am no longer maintaining this bot. This project is as-is, and I will not provide any help if you have questions. 
Sincerely, 
Staretta


DokuBot - A guide. Probably.

You'll need maven to download the required libraries. Just clone the repository, run maven_update.bat from the DokuBot directory, after getting maven installed, and if you still have problems, verify that you added M2_REPO variable in eclipse, and check the build path of the project under src to see if it's disallowing */*.java and remove it. 

After that it should run just fine. 

