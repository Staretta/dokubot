package net.one337ism.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MiscUtil {

    /**
     * Outputs a formatted date based on the current time.
     * 
     * @return a formatted string like this: "MM/dd/yy HH:mm:ss"
     */
    public static String date() {
        // Set up the date stuff. Seems a bit redundant though. Argh.
        String sDate = null;
        Date date = new Date();
        sDate = new SimpleDateFormat("MM/dd/yy HH:mm:ss").format(date);

        return sDate;
    }

}