package net.one337ism.util;

import java.util.Iterator;
import java.util.Set;

import net.one337ism.DokuBot;

import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ircUtil {

    // Set up the logger stuff
    private static Logger logger = LoggerFactory.getLogger(DokuBot.class);

    /**
     * Sends a message to a channel
     * 
     * @param event
     *            MessageEvent
     * @param message
     *            we want to send
     */
    public static void sendMessage(MessageEvent event, String message) {
        event.getChannel().send().message(message);
    }

    /**
     * Sends a Private Message to a user
     * 
     * @param event
     *            PrivateMessageEvent
     * @param message
     *            we want to send
     */
    public static void sendMessage(PrivateMessageEvent event, String message) {
        event.getUser().send().message(message);
    }

    /**
     * Sends a notice message to target user
     * 
     * @param event
     *            MessageEvent
     * @param target
     *            who to send the message to
     * @param message
     *            we want to send
     */
    public static void sendNotice(MessageEvent event, String message) {
        event.getUser().send().notice(message);
    }

    /**
     * Sends a notice message to target user
     * 
     * @param event
     *            PrivateMessageEvent
     * @param target
     *            who to send the message to
     * @param message
     *            we want to send
     */
    public static void sendNotice(PrivateMessageEvent event, String target, String message) {
        event.getUser().send().notice(message);
    }

    /**
     * Checks to see if user is an operator of the specified channel.
     * 
     * @param event
     *            MessageEvent
     * @param channel
     *            Channel we want to check
     * @return boolean
     * 
     */
    public static boolean isOP(MessageEvent event, String channel) {
        // See if user is an operator of the specified channel.

        // Initialize the variable.
        boolean isOP = false;

        // Get list of operators in a channel.
        Set<User> operators = event.getChannel().getOps();
        Iterator<User> itr = operators.iterator();
        // Step through the set, and see if the user is an operator.
        while (itr.hasNext()) {
            if (itr.next().getNick().equalsIgnoreCase(event.getUser().getNick()))
                isOP = true;
        }

        return isOP;
    }

    /**
     * Checks to see if user is an operator of the specified channel.
     * 
     * @param event
     *            PrivateMessageEvent
     * @param channel
     *            Channel we want to check
     * @return boolean
     * 
     */
    public static boolean isOP(PrivateMessageEvent event, String channel) {
        // Initialize the variable.
        boolean isOP = false;

        // Need to go about this a different way. By getting all channels that the user is an operator in.
        Set<Channel> channels = event.getUser().getChannelsOpIn();
        Iterator<Channel> itr = channels.iterator();
        while (itr.hasNext()) {
            Channel chan = itr.next();
            if (chan.getName().equalsIgnoreCase(channel) && chan.isOp(event.getUser())) {
                isOP = true;
            }
        }

        return isOP;
    }
}