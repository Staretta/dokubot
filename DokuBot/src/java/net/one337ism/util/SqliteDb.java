package net.one337ism.util;

import net.one337ism.DokuBot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqliteDb extends Db {
    // Set up the logger stuff
    private static Logger logger = LoggerFactory.getLogger(DokuBot.class);

    public SqliteDb(String sDriverKey, String sUrlKey) {
        try {
            init(sDriverKey, sUrlKey);
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        if (conn != null) {
            // logger.debug("Connected OK using " + sDriverKey + " to " + sUrlKey);
        } else {
            logger.debug("Connection failed for" + sDriverKey + " to " + sUrlKey);
        }
    }

    public static String sqlQuote(String i) {
        // Just a simple method to encapsulate a string with quotes that are destined to be used in an SQL statement.
        return "'" + i + "'";
    }
}