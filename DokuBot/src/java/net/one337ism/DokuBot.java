package net.one337ism;

import net.one337ism.modules.Greeter;
import net.one337ism.modules.RateLimiter;
import net.one337ism.util.ircUtil;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.pircbotx.hooks.events.QuitEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DokuBot extends ListenerAdapter implements Listener {
    // slf4j Stuff
    private static Logger logger       = LoggerFactory.getLogger(DokuBot.class);

    // Config File
    static Config         cfg          = new Config();
    static String         irc_server   = cfg.getProperty("irc_server");
    static int            irc_port     = Integer.parseInt(cfg.getProperty("irc_port"));
    public static String  irc_channel  = cfg.getProperty("irc_channel");
    private static String irc_nickname = cfg.getProperty("irc_nickname");
    private static String irc_username = cfg.getProperty("irc_username");
    public static String  bot_owner    = cfg.getProperty("bot_owner");
    private static String bot_version  = cfg.getProperty("bot_version");
    private static String bot_password = cfg.getProperty("bot_password");

    @Override
    public void onPrivateMessage(PrivateMessageEvent event) throws Exception {
        if ((event.getUser().getNick().equalsIgnoreCase(bot_owner) || ircUtil.isOP(event, irc_channel))
                && event.getMessage().equalsIgnoreCase("!quit")) {
            // Shutdown upon receiving a quit command from either the bot owner, or a channel operator
            ircUtil.sendMessage(event, "Shutting Down.");
            event.getBot().stopBotReconnect();
            event.getBot().sendIRC().quitServer();
        }
    }

    @Override
    public void onConnect(ConnectEvent event) throws Exception {
        if (!bot_password.isEmpty()) {
            // TODO: replace with identify at some point.
            logger.info("(" + event.getBot().getNick() + "->NickServ) IDENTIFY " + "PASSWORD_HERE");
            event.getBot().sendIRC().message("NickServ", "IDENTIFY " + bot_password);
        }
    }

    @Override
    public void onQuit(QuitEvent event) throws Exception {
        // If we see our default nickname quit, then rename our name to it.
        if (event.getUser().getNick().equalsIgnoreCase(irc_nickname)) {
            event.getBot().sendIRC().changeNick(irc_nickname);
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        // Load the sqlite-JDBC and mysql-JDBC driver using the current class loader
        Class.forName("org.sqlite.JDBC");
        Class.forName("com.mysql.jdbc.Driver");

        // @formatter:off
        // Configuration builder
        Configuration configuration = new Configuration.Builder()
                .setName(irc_nickname)           // Bot's Nickname "nickname!~username@hostname"
                .setLogin(irc_username)          // Bot's Username
                .setRealname(bot_version)        // Version/Realname - Note: setVersion isn't working in 2.0 anymore.
                .setAutoNickChange(true)         // If the nickname is taken, changes nickname
                .setAutoReconnect(true)          // Reconnects on disconnect and sometimes netsplits
                .setCapEnabled(true)             // Enables certain server protocols
                .setIdentServerEnabled(false)    // Ident server - Doesn't work too well in 2.0
                .setServerHostname(irc_server)   // Server
                .setServerPort(irc_port)         // Server's port
                .addAutoJoinChannel(irc_channel) // Channel we want to join on connect
                .addListener(new RateLimiter())  // Add a Ratelimiter module
                .addListener(new DokuBot())      // Add some basic functions for the bot
                .addListener(new Greeter())      // Adds greeter functions
                .buildConfiguration();
        PircBotX bot = new PircBotX(configuration);
        // @formatter:on

        try {
            bot.startBot();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
