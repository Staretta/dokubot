package net.one337ism.modules;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.one337ism.DokuBot;
import net.one337ism.util.SqliteDb;
import net.one337ism.util.ircUtil;

import org.apache.commons.dbutils.DbUtils;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Faq extends ListenerAdapter {
    // TODO: Add delete all FAQ.

    // Set up the logger stuff
    private static Logger logger     = LoggerFactory.getLogger(DokuBot.class);

    // Database stuff. Should probably move to the config file at some point.
    private String        dbUrl      = "jdbc:sqlite:data/faq.db";
    private String        dbDriver   = "org.sqlite.JDBC";

    // Creates the database tables if they haven't been created yet.
    private boolean       tableExist = createTableIfNotExist();

    // Going with a private error code, because I can't think of a better way to do error reporting, atm.
    private byte          error      = 0;

    private SqliteDb dbConnect() {
        // Open a connection to the database.
        SqliteDb db = new SqliteDb(dbDriver, dbUrl);
        return db;
    }

    private boolean createTableIfNotExist() {
        // Set up the connection,
        SqliteDb db = dbConnect();
        Connection conn = db.getConnection();
        try {
            DatabaseMetaData dbm = conn.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "FAQ", null);
            if (tables.next()) {
                // Table exists
                // logger.info("Table Greeting exists");
                return true;
            } else {
                // Table doesn't exist
                logger.info("Table FAQ doesn't exist.");
                db.executeStmt("CREATE TABLE FAQ (ID INTEGER PRIMARY KEY, Line TEXT, Nickname TEXT, Date TEXT)");
                logger.info("Created table.");
                return true;
            }
        } catch (SQLException pass) {
            return false;
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

    private boolean addFaq(String line, String nickname, String date) {
        // Set up the connection, and the statement.
        SqliteDb db = dbConnect();
        Connection conn = db.getConnection();
        PreparedStatement insFaq = null;
        try {
            insFaq = conn.prepareStatement("INSERT INTO FAQ ( 'Line', 'Nickname', 'Date' ) VALUES (?,?,?)");
            insFaq.setString(1, line);
            insFaq.setString(2, nickname);
            insFaq.setString(3, date);
            insFaq.execute();
            return true;
        } catch (SQLException ex) {
            error = 3;
        } finally {
            DbUtils.closeQuietly(insFaq);
            DbUtils.closeQuietly(conn);
        }
        return false;
    }

    private boolean delFaq(String ID) {
        // Set up the connection, and some variables
        SqliteDb db = dbConnect();
        Connection conn = db.getConnection();
        PreparedStatement selFaq = null;
        PreparedStatement delFaq = null;
        ResultSet rs = null;

        try {
            // Try to parse input to see if it's a valid number
            int inputNumber = Integer.parseInt(ID);

            // Now we need to see if there the number relates to a greeting in the db
            selFaq = conn.prepareStatement("SELECT * FROM FAQ WHERE ID = ?");
            selFaq.setInt(1, inputNumber);
            rs = selFaq.executeQuery();

            if (rs.isBeforeFirst()) {
                delFaq = conn.prepareStatement("DELETE FROM FAQ WHERE ID = ?");
                delFaq.setInt(1, inputNumber);
                delFaq.execute();
                return true;
            } else {
                error = 2;
            }
        } catch (SQLException ex) {
            error = 3;
        } catch (NumberFormatException nfe) {
            error = 4;
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(selFaq);
            DbUtils.closeQuietly(delFaq);
            DbUtils.closeQuietly(conn);
        }
        return false;
    }

    private List<String> getFaq() {
        return getFaq(false);
    }

    private List<String> getFaq(boolean operator) {
        // Set up the connection and some variables
        SqliteDb db = dbConnect();
        Connection conn = db.getConnection();
        ResultSet rs = null;
        PreparedStatement selFaq = null;

        try {
            selFaq = conn.prepareStatement("SELECT * FROM FAQ ORDER BY ID");
            rs = selFaq.executeQuery();

            if (rs.isBeforeFirst()) {
                List<String> message = rsParser(rs, operator);
                return message;
            } else {
                error = 2;
            }
        } catch (SQLException ex) {
            error = 3;
        } finally {
            DbUtils.closeQuietly(conn, selFaq, rs);
        }
        return null;
    }

    private String getFaq(String ID) {
        // Set up the connection and the ResultSet
        SqliteDb db = dbConnect();
        Connection conn = db.getConnection();
        ResultSet rs = null;
        PreparedStatement selFaq = null;

        try {
            // Parse input to see if it's a valid number
            int inputNumber = Integer.parseInt(ID);

            selFaq = conn.prepareStatement("SELECT * FROM Greeting WHERE ID = ?");
            selFaq.setInt(1, inputNumber);
            rs = selFaq.executeQuery();

            if (rs.isBeforeFirst()) {
                List<String> message = rsParser(rs, true);
                Object[] finalMsg = message.toArray();
                return finalMsg[0].toString();
            } else {
                error = 2;
            }
        } catch (SQLException ex) {
            error = 3;
        } catch (NumberFormatException nfe) {
            error = 4;
        } finally {
            DbUtils.closeQuietly(conn, selFaq, rs);
        }
        return null;
    }

    /**
     * Parses a result set containing entries for Greetings.
     * 
     * @param resultSet
     *            A result set containing ID, Nickname, Line, and Date
     * @throws SQLException
     *             If any Exceptions might be thrown, throw them up and let MessageEvent handle it
     * @return a list of lines to be sent to the user
     * 
     */
    private List<String> rsParser(ResultSet resultSet) throws SQLException {
        return rsParser(resultSet, false);
    }

    /**
     * Parses a result set containing entries for Greetings.
     * 
     * @param resultSet
     *            A result set containing ID, Nickname, Line, and Date
     * @param operator
     *            if the user is an operator, we want to display data differently
     * @throws SQLException
     *             If any Exceptions might be thrown, throw them up
     * @return a list of lines to be sent to the user
     * 
     */
    private List<String> rsParser(ResultSet resultSet, boolean operator) throws SQLException {
        // Function to build a nice list from the results of the various searches.
        List<String> message = new ArrayList<>();
        while (resultSet.next()) {
            int ID = resultSet.getInt("ID");
            String line = resultSet.getString("Line");
            String nickname = resultSet.getString("Nickname");
            String date = resultSet.getString("Date");
            if (operator) {
                message.add("[" + date + "] ID: " + ID + " | " + nickname + ": " + line);
            } else {
                message.add(line);
            }
        }
        return message;
    }

    @Override
    public void onMessage(MessageEvent event) throws Exception {
        // If message starts with !faq
        if (event.getMessage().trim().toLowerCase().startsWith("!faq")
                && !RateLimiter.isRateLimited(event.getUser().getNick())) {
            // Split the message into parameters.
            String[] param = event.getMessage().trim().split("\\s", 3);

            if (param.length == 1) {
                // They entered !faq, and so we send them ALL the faq. >.<
                List<String> faq = getFaq();
                if (faq != null) {
                    Iterator itr = faq.iterator();
                    while (itr.hasNext()) {
                        ircUtil.sendNotice(event, itr.next().toString());
                    }
                }
            } else if (param[1].equalsIgnoreCase("-help") || param[1].equalsIgnoreCase("-h")) {
                // If they entered !faq -help, send them help info!
                String[] help = { "!faq : Displays all the current Dokucraft faq.",
                        "!faq -help : Displays this help message!",
                        "!faq <search> : Displays faq related to your search terms." };
                for (int i = 0; i < help.length; i++) {
                    ircUtil.sendNotice(event, help[i]);
                }
            } else {
                // Search terms here. Not yet implemented.
            }
        }
    }

    @Override
    public void onPrivateMessage(PrivateMessageEvent event) throws Exception {
        // If the message starts with !faq and the user is an operator of the channel
        if (event.getMessage().trim().toLowerCase().startsWith("!faq") && ircUtil.isOP(event, DokuBot.irc_channel)) {
            // Split the message into parameters.
            String[] param = event.getMessage().trim().split("\\s", 3);

            if (param.length == 1) {

            }
        }
    }
}